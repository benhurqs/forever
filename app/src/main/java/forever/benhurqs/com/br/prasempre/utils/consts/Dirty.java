package forever.benhurqs.com.br.prasempre.utils.consts;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Benhur on 20/11/16.
 */

public class Dirty {
    public static final int NEW = 0;
    public static final int UPDATED = 1;


    @IntDef({NEW, UPDATED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {}
}
