package forever.benhurqs.com.br.prasempre.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import forever.benhurqs.com.br.prasempre.BuildConfig;

/**
 * Created by Benhur on 07/11/16.
 */

public class Utils {

    public static final String DATE_HOUR_FORMAT = "dd/MM/yyyy HH:mm";
    private static String TAG = "FOREVER";

    public static void dLog(String msg){
        if(BuildConfig.SHOW_LOG){
            Log.e(TAG, msg);
        }
    }

    public static boolean isEmpty(String string) {
        return (string==null || "".equals(string.trim()) || "null".equals(string.toLowerCase()));
    }

    public static boolean needSync(long time){
        Calendar today = Calendar.getInstance();
        if(time == 0){
            return true;
        }

        long diff = today.getTimeInMillis() - time;
        long min = diff / ( 60 * 1000);
        long hours = diff/ (60 * 60 * 1000);
        long days = diff / (24 * 60 * 60 * 1000);

        boolean validad = false;

        if(BuildConfig.SHOW_LOG){
            //Quando for em modo debug atualiza de 5 em 5 minutos
            validad = (min >= 1 );
        }else{
            validad = (hours >= 20);
        }

        if(validad){
            return true;
        }

        return false;
    }

    /**
            * @return Retorna o horário no formato HH:mm
    */
    public static String getCurrentTime(){
        //1987-07-03T00:00:00.0000000-03:00
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_HOUR_FORMAT);
        String currentDateandTime = sdf.format(new Date());
        return currentDateandTime;
    }

}
