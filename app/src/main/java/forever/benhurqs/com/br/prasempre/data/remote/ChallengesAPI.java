package forever.benhurqs.com.br.prasempre.data.remote;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import forever.benhurqs.com.br.prasempre.data.DataContract;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.repository.LocalRepository;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Benhur on 16/10/16.
 */

public class ChallengesAPI implements DataContract {

    public static final String TABLE = "Challenges";
    public static final String ID_COLUMN = "id";
    public static final String TITLE_COLUMN = "title";
    public static final String DESC_COLUMN = "description";
    public static final String QUESTION_COLUMN = "question";

    public static ChallengesAPI instance;

    public static ChallengesAPI getInstance() {
        if (instance == null) {
            instance = new ChallengesAPI();
        }
        return instance;
    }

    /**
     * Save challenge object in DB using Parse
     **/
    @Override
    public void save(ChallengeObj obj) {
//        ParseObject testObject = new ParseObject(TABLE);
//        testObject.put(ID_COLUMN, obj.id);
//        testObject.put(TITLE_COLUMN, obj.title);
//        testObject.put(DESC_COLUMN, obj.description);
//        testObject.put(QUESTION_COLUMN, obj.question);
//
//        testObject.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                Log.e("Checguei","Aqui -> " + e);
//            }
//        });

        FirebaseDatabase.getInstance().getReference().child(TABLE).push().setValue(obj);
        FirebaseDatabase.getInstance().getReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Utils.dLog("aqui - salvei");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utils.dLog(databaseError.getMessage());
            }
        });


    }


    /**
     * Save challenge list in DB using Parse
     **/
    @Override
    public void save(List<ChallengeObj> objList) {
        for (ChallengeObj obj : objList) {
            save(obj);
        }
//        FirebaseDatabase.getInstance().getReference();


    }

    /**
     * Find all challenge rows in DB using Parse
     **/
    @Override
    public Observable<List<ChallengeObj>> findAll() {
        Observable.OnSubscribe<List<ChallengeObj>> subscribe = new Observable.OnSubscribe<List<ChallengeObj>>() {
            @Override
            public void call(final Subscriber<? super List<ChallengeObj>> subscriber) {
//                ParseQuery<ParseObject> query = ParseQuery.getQuery(TABLE);
//                query.findInBackground(new FindCallback<ParseObject>() {
//                    @Override
//                    public void done(List<ParseObject> list, ParseException e) {
//                        if (e == null) {
//                            List<ChallengeObj> challengeObjList = new ArrayList<ChallengeObj>();
//                            for (int i = 0; i < list.size(); i++) {
//                                challengeObjList.add(convertToObj(list.get(i)));
//                            }
//
//                            //Save local storage
//                            LocalRepository.getInstance().saveChallenge(challengeObjList);
//                            subscriber.onNext(challengeObjList);
//                            subscriber.onCompleted();
//                        }
//                    }
//                });

                FirebaseDatabase.getInstance().getReference().child(TABLE).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //init list
                        List<ChallengeObj> challengeObjList = new ArrayList<ChallengeObj>();
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            try {

                                ChallengeObj obj = postSnapshot.getValue(ChallengeObj.class);
                                obj.key =  postSnapshot.getKey();
                                challengeObjList.add(obj);

                            } catch (Exception e) {
                                subscriber.onError(new Throwable(e.getMessage()));
                            }
                        }

                        //Save local storage
                        LocalRepository.getInstance().saveChallenge(challengeObjList);
                        subscriber.onNext(challengeObjList);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utils.dLog(databaseError.getMessage());
                        subscriber.onError(new Throwable(databaseError.getMessage()));
                    }
                });
            }
        };

        return Observable.create(subscribe);
    }

    /**
     * Find challenge rows in DB by ID using Parse
     **/
    @Override
    public Observable<ChallengeObj> findByKey(final String key) {
        Observable.OnSubscribe<ChallengeObj> subscribe = new Observable.OnSubscribe<ChallengeObj>() {
            @Override
            public void call(final Subscriber<? super ChallengeObj> subscriber) {
//                ParseQuery<ParseObject> query = ParseQuery.getQuery(TABLE);
//                query.whereEqualTo(ID_COLUMN, id);
//                query.findInBackground(new FindCallback<ParseObject>() {
//                    @Override
//                    public void done(List<ParseObject> list, ParseException e) {
//                        if (e == null) {
//                            if (list.size() > 0) {
//                                ChallengeObj challengeObj = ChallengeObj.convertToObj(list.get(0));
//                                //Save local storage
//                                LocalRepository.getInstance().saveChallenge(challengeObj);
//                                subscriber.onNext(challengeObj);
//                                subscriber.onCompleted();
//                            }
//
//                        }
//                    }
//                });

                FirebaseDatabase.getInstance().getReference()
                        .child(TABLE)
                        .child(key)
                        .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        ChallengeObj challengeObj = dataSnapshot.getValue(ChallengeObj.class);
                        //Save local storage
                        LocalRepository.getInstance().saveChallenge(challengeObj);
                        subscriber.onNext(challengeObj);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utils.dLog(databaseError.getMessage());
                        subscriber.onError(new Throwable(databaseError.getMessage()));
                    }
                });


            }
        };

        return Observable.create(subscribe);
    }

    /**
     * Find challenge rows in DB by ID using Parse
     **/
    @Override
    public Observable<ChallengeObj> findById(final int id) {
        Observable.OnSubscribe<ChallengeObj> subscribe = new Observable.OnSubscribe<ChallengeObj>() {
            @Override
            public void call(final Subscriber<? super ChallengeObj> subscriber) {

                FirebaseDatabase.getInstance().getReference().child(TABLE).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            try {

                                ChallengeObj obj = postSnapshot.getValue(ChallengeObj.class);
                                obj.key =  postSnapshot.getKey();
                                if(obj.id == id){
                                    subscriber.onNext(obj);
                                    subscriber.onCompleted();
                                    return;
                                }

                            } catch (Exception e) {
                                subscriber.onError(new Throwable(e.getMessage()));
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utils.dLog(databaseError.getMessage());
                        subscriber.onError(new Throwable(databaseError.getMessage()));
                    }
                });

            }
        };

        return Observable.create(subscribe);
    }


}
