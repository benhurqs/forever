package forever.benhurqs.com.br.prasempre.base;

/**
 * Created by Benhur on 21/10/16.
 */

public interface BasePresenter {
    void subscribe();
    void unsubscribe();
}
