package forever.benhurqs.com.br.prasempre.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

import forever.benhurqs.com.br.prasempre.utils.Utils;

/**
 * Created by Benhur on 07/11/16.
 */

public class UserPreference {
    private Context context;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static UserPreference instance;

    public static UserPreference getInstance(Context context){
        if(instance == null){
            instance = new UserPreference(context);
        }

        return instance;
    }

    public UserPreference(Context context) {
        this.context = context;
        pref = context.getSharedPreferences("user_pref", Context.MODE_MULTI_PROCESS);
    }


    /**
     * Get last challenge  view
     *
     * @return challengeId
     *
     */
    public int getLastChallenge() {
        return pref.getInt("user_challenge", -1);
    }

    /**
     * Get last challenge key view
     *
     * @return key
     *
     */
    public String getLastChallengeKey() {
        return pref.getString("user_challenge_key", null);
    }

    /**
     * Save the last challenge opened
     *
     * @param challenge
     */
    public void saveLastChallenge(int challenge) {
        editor = pref.edit();
        editor.putInt("user_challenge", challenge);
        editor.commit();
    }

    /**
     * Save the last challenge key opened
     *
     * @param key
     */
    public void saveLastChallengeKey(String key) {
        editor = pref.edit();
        editor.putString("user_challenge_key", key);
        editor.commit();
    }

    /**
     * Save the first time that user read the challenge
     */
    public void saveLastView(){
        editor = pref.edit();
        editor.putLong("user_time_view", Calendar.getInstance().getTimeInMillis());
        editor.putString("user_time_last_view", Utils.getCurrentTime());
        editor.commit();
    }

    public String getLastDateView(){
        return pref.getString("user_time_last_view", "null");
    }



    /**
     * Get the first time that user read the challenge
     */
    public long getLastView(){
        return pref.getLong("user_time_view", 0);
    }

}
