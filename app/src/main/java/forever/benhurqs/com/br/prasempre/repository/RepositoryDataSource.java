package forever.benhurqs.com.br.prasempre.repository;

import java.util.List;

import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import rx.Observable;

/**
 * Created by Benhur on 26/10/16.
 */

public interface RepositoryDataSource {
    Observable<List<ChallengeObj>> getAllChallenges();
    Observable<ChallengeObj> getChallengeByKey(String key);
    Observable<ChallengeObj> getChallengeById(int id);
    void saveChallenge(List<ChallengeObj> challengeObj);
    void saveChallenge(ChallengeObj challengeObj);
    Observable<UserObj> saveUser(UserObj user);
    Observable<UserObj> loginUser(UserObj user);
    Observable<AnswerObj> saveAnswer(AnswerObj answerObj);
}
