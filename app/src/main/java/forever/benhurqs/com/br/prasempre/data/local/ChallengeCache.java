package forever.benhurqs.com.br.prasempre.data.local;

import android.util.Log;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import forever.benhurqs.com.br.prasempre.cache.Cache;
import forever.benhurqs.com.br.prasempre.cache.CacheDAO;
import forever.benhurqs.com.br.prasempre.cache.CacheObj;
import forever.benhurqs.com.br.prasempre.data.DataContract;
import forever.benhurqs.com.br.prasempre.listener.OnSaveCacheListener;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Benhur on 26/10/16.
 */

public class ChallengeCache implements DataContract {

    private static ChallengeCache instance;

    public static ChallengeCache getInstance() {
        if (instance == null) {
            instance = new ChallengeCache();
        }

        return instance;
    }

    @Override
    public Observable<List<ChallengeObj>> findAll() {
        Observable.OnSubscribe<List<ChallengeObj>> subscribe = new Observable.OnSubscribe<List<ChallengeObj>>() {
            @Override
            public void call(final Subscriber<? super List<ChallengeObj>> subscriber) {
                CacheObj cacheObj = CacheDAO.getInstance().findByType(Cache.CHALLENGE_LIST);
                if (cacheObj == null) {
                    return;
                }

                ChallengeObj[] challengeObjList = cacheObj.convertToObject(ChallengeObj[].class);
                if (challengeObjList == null ) {
                    return;
                }

                subscriber.onNext(Arrays.asList(challengeObjList));
                subscriber.onCompleted();
            }
        };

        return Observable.create(subscribe);
    }


    @Override
    public Observable<ChallengeObj> findByKey(final String key) {
        Observable.OnSubscribe<ChallengeObj> subscribe = new Observable.OnSubscribe<ChallengeObj>() {
            @Override
            public void call(Subscriber<? super ChallengeObj> subscriber) {
                CacheObj cacheObj = CacheDAO.getInstance().findByType(Cache.CHALLENGE_LIST);
                if (cacheObj == null) {
                    return;
                }

                ChallengeObj[] challengeObjList = cacheObj.convertToObject(ChallengeObj[].class);
                if (challengeObjList == null ) {
                    return;
                }

                for(ChallengeObj challengeObj : challengeObjList){
                    if(challengeObj.key.equalsIgnoreCase(key)){
                        subscriber.onNext(challengeObj);
                        subscriber.onCompleted();
                        return;
                    }
                }


            }

        };

        return Observable.create(subscribe);
    }

    public Observable<ChallengeObj> findById(final int id) {
        Observable.OnSubscribe<ChallengeObj> subscribe = new Observable.OnSubscribe<ChallengeObj>() {
            @Override
            public void call(Subscriber<? super ChallengeObj> subscriber) {
                CacheObj cacheObj = CacheDAO.getInstance().findByType(Cache.CHALLENGE_LIST);
                if (cacheObj == null) {
                    return;
                }

                ChallengeObj[] challengeObjList = cacheObj.convertToObject(ChallengeObj[].class);
                if (challengeObjList == null ) {
                    return;
                }

                for(ChallengeObj challengeObj : challengeObjList){
                    if(challengeObj.id == id){
                        subscriber.onNext(challengeObj);
                        subscriber.onCompleted();
                        return;
                    }
                }


            }

        };

        return Observable.create(subscribe);
    }


    @Override
    public void save(List<ChallengeObj> challengeObj) {
        Gson gson = new Gson();
        String cache = gson.toJson(challengeObj);
        CacheDAO.getInstance().save(new CacheObj(cache, Cache.CHALLENGE_LIST), new OnSaveCacheListener() {
            @Override
            public void onSuccess() {
                Log.e("Salvo", "Tudo salvo com sucesso");
            }

            @Override
            public void onError(String error) {
                Log.e("error", error);
            }
        });
    }

    @Override
    public void save(ChallengeObj challengeObj) {
        Gson gson = new Gson();
        String cache = gson.toJson(challengeObj);
        CacheDAO.getInstance().save(new CacheObj(cache, Cache.CHALLENGE), new OnSaveCacheListener() {
            @Override
            public void onSuccess() {
                Log.e("Salvo", "Tudo salvo com sucesso");
            }

            @Override
            public void onError(String error) {
                Log.e("error", error);
            }
        });
    }
}
