package forever.benhurqs.com.br.prasempre.activity.cadastre;

import android.app.Activity;

import forever.benhurqs.com.br.prasempre.base.BasePresenter;
import forever.benhurqs.com.br.prasempre.base.BaseView;

/**
 * Created by Benhur on 13/11/16.
 */

public class CadastreContract {

    interface View extends BaseView<CadastreContract.Presenter> {

        void hideProgress();
        void showProgress();
        String getSex();
        String getName();
        String getEmail();
        Activity getContext();
        void showError(String msg);

    }

    interface Presenter extends BasePresenter {
        void saveUser();
    }
}
