package forever.benhurqs.com.br.prasempre.repository;

import java.util.List;

import forever.benhurqs.com.br.prasempre.data.remote.AnswerAPI;
import forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI;
import forever.benhurqs.com.br.prasempre.data.remote.UserAPI;
import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import rx.Observable;

/**
 * Created by Benhur on 26/10/16.
 */

public class ParseRepository implements RepositoryDataSource {

    private static ParseRepository instance;

    public static ParseRepository getInstance(){
        if(instance == null){
            instance = new ParseRepository();
        }

        return instance;
    }

    @Override
    public Observable<List<ChallengeObj>> getAllChallenges() {
        return ChallengesAPI.getInstance().findAll();
    }

    @Override
    public Observable<ChallengeObj> getChallengeByKey(String key) {
        return ChallengesAPI.getInstance().findByKey(key);
    }

    @Override
    public Observable<ChallengeObj> getChallengeById(int id) {
        return ChallengesAPI.getInstance().findById(id);
    }

    @Override
    public void saveChallenge(List<ChallengeObj> challengeObj) {
        ChallengesAPI.getInstance().save(challengeObj);
    }

    @Override
    public void saveChallenge(ChallengeObj challengeObj) {
        ChallengesAPI.getInstance().save(challengeObj);
    }

    @Override
    public Observable<UserObj> saveUser(UserObj user) {
        return UserAPI.getInstance().save(user);
    }

    @Override
    public Observable<UserObj> loginUser(UserObj user) {
//        return UserAPI.getInstance().login(user.userName);
        return null;
    }

    @Override
    public Observable<AnswerObj> saveAnswer(AnswerObj answerObj) {
        return AnswerAPI.getInstance().save(answerObj);
    }
}
