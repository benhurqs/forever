package forever.benhurqs.com.br.prasempre.alarm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import forever.benhurqs.com.br.prasempre.R;

/**
 * Created by Benhur on 10/11/16.
 */

public class AlarmHelper {

    public static class Builder {
        private String title = "PraSempre", text = "PraSempre";
        private Context context;
        private Class activityClass;
        private int time;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setContentTitle(String contentTitle) {
            this.title = contentTitle;
            return this;
        }

        public Builder setContentText(String contentText) {
            this.text = contentText;
            return this;
        }

        public Builder setScreenAfterClick(Class activity) {
            this.activityClass = activity;
            return this;
        }

        public Builder setTimeInHours(int timeInHours){
            this.time = timeInHours * 60 * 60 * 1000;
            return this;
        }

        public Builder setTimeInSecounds(int timeInMinutes){
            this.time = timeInMinutes *  1000;
            return this;
        }

        public Builder setTimeInMinutes(int timeInMinutes){
            this.time = timeInMinutes * 60 * 1000;
            return this;
        }

        public void build() {
            Intent notificationIntent = new Intent(context, AlarmManagerBroadcastReceiver.class);
            notificationIntent.putExtra(AlarmManagerBroadcastReceiver.NOTIFICATION_ID, 1);
            notificationIntent.putExtra(AlarmManagerBroadcastReceiver.NOTIFICATION, getNotification());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            long futureInMillis = SystemClock.elapsedRealtime() + time;
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        }

        private Notification getNotification() {
            Notification.Builder builder = new Notification.Builder(context);
            builder.setContentTitle(title);
            builder.setContentText(text);
            builder.setDefaults(Notification.DEFAULT_ALL);
            builder.setAutoCancel(true);
            builder.setSmallIcon(R.drawable.ic_notification);

            if(activityClass != null) {
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, new Intent(context, activityClass), 0);
                builder.setContentIntent(pendingIntent);
            }

            return builder.build();

        }


    }
}
