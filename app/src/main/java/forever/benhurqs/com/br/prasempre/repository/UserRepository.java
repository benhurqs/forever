package forever.benhurqs.com.br.prasempre.repository;

import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 13/11/16.
 */

public class UserRepository {

    private static UserRepository instance;
    private RepositoryDataSource localRepository;
    private RepositoryDataSource remoteRepository;

    public static UserRepository getInstance(RepositoryDataSource localRepository, RepositoryDataSource remoteRepository) {
        if (instance == null) {
            instance = new UserRepository(localRepository, remoteRepository);
        }

        return instance;
    }

    public UserRepository(@NonNull RepositoryDataSource localRepository, @NonNull RepositoryDataSource remoteRepository) {
        this.localRepository = checkNotNull(localRepository);
        this.remoteRepository = checkNotNull(remoteRepository);
    }

    /**
     * Get auth user
     *
     * @param context
     * @param user
     * @return Observable
     */
//    public Observable<UserObj> login(Context context, final UserObj user) {
//        Observable<UserObj> localObserver = localRepository.loginUser(user)
//                .flatMap(new Func1<UserObj, Observable<UserObj>>() {
//                    @Override
//                    public Observable<UserObj> call(UserObj userObj) {
//                        return Observable.just(userObj);
//                    }
//                });
//
//        Observable<UserObj> remoteObserver = remoteRepository.loginUser(user)
//                .flatMap(new Func1<UserObj, Observable<UserObj>>() {
//                    @Override
//                    public Observable<UserObj> call(UserObj userObj) {
//                        return Observable.just(userObj);
//                    }
//                });
//
//
//        return Observable.merge(localObserver, remoteObserver).first()
//                .map(new Func1<UserObj, UserObj>() {
//                    @Override
//                    public UserObj call(UserObj UserObj) {
//                        if (UserObj == null) {
//                            throw new NoSuchElementException("No User found with userName " + user.userName);
//                        }
//                        return UserObj;
//                    }
//                });
//    }

    /**
     * Save user in Parse and Local
     *
     * @param context
     * @param user
     * @return Observable
     */
//    public Observable<UserObj> save(Context context, final UserObj user) {
//        Observable<UserObj> localObserver = localRepository.saveUser(user)
//                .flatMap(new Func1<UserObj, Observable<UserObj>>() {
//                    @Override
//                    public Observable<UserObj> call(UserObj userObj) {
//                        return Observable.just(userObj);
//                    }
//                });
//
//        Observable<UserObj> remoteObserver = remoteRepository.saveUser(user)
//                .flatMap(new Func1<UserObj, Observable<UserObj>>() {
//                    @Override
//                    public Observable<UserObj> call(UserObj userObj) {
//                        return Observable.just(userObj);
//                    }
//                });
//
//
//        return Observable.concat(remoteObserver, localObserver).first()
//                .map(new Func1<UserObj, UserObj>() {
//                    @Override
//                    public UserObj call(UserObj UserObj) {
//                        if (UserObj == null) {
//                            throw new NoSuchElementException("No User found with userName " + user.userName);
//                        }
//                        return UserObj;
//                    }
//                });
//    }


}
