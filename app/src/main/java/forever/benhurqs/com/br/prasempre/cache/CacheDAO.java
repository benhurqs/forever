package forever.benhurqs.com.br.prasempre.cache;

import forever.benhurqs.com.br.prasempre.listener.OnSaveCacheListener;
import io.realm.Realm;

/**
 * Created by Benhur on 16/10/16.
 */

public class CacheDAO {

    protected Realm realm;

    private static CacheDAO instance;

    public static CacheDAO getInstance(){
        if(instance == null){
            instance = new CacheDAO();
        }

        return instance;
    }

    public CacheDAO() {
        realm = Realm.getDefaultInstance();
    }

    /**
     * Save object on DataBase
     **/
    public void save(final CacheObj object, final OnSaveCacheListener listener) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                CacheObj newObj = bgRealm.where(CacheObj.class)
                        .equalTo("type", object.type)
                        .findFirst();

                if(newObj == null){
                    newObj = bgRealm.createObject(CacheObj.class);
                }

                newObj.cache = object.cache;
                newObj.type = object.type;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (listener != null) {
                    listener.onError(error.getMessage());
                }
            }
        });
    }


    public CacheObj findByType(@Cache.Type int type){
        return realm.where(CacheObj.class)
                .equalTo("type", type)
                .findFirst();
    }
}
