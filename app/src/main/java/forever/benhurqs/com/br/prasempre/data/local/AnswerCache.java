package forever.benhurqs.com.br.prasempre.data.local;

import android.util.Log;

import java.util.List;

import forever.benhurqs.com.br.prasempre.listener.OnSaveCacheListener;
import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.obj.user.AnswerDAO;
import forever.benhurqs.com.br.prasempre.utils.consts.Dirty;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Benhur on 20/11/16.
 */

public class AnswerCache {

    private static AnswerCache instance;

    public static AnswerCache getInstance() {
        if (instance == null) {
            instance = new AnswerCache();
        }

        return instance;
    }

    public Observable<AnswerObj> save(final AnswerObj answerObj) {
        Observable.OnSubscribe<AnswerObj> subscribe =  new Observable.OnSubscribe<AnswerObj>() {
            @Override
            public void call(final Subscriber<? super AnswerObj> subscriber) {
                AnswerDAO.getInstance().save(answerObj, new OnSaveCacheListener() {
                    @Override
                    public void onSuccess() {
                        subscriber.onNext(answerObj);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onError(String error) {
                        subscriber.onError(new Throwable(error));
                    }
                });
            }
        };

        return Observable.create(subscribe);

    }

    public Observable<List<AnswerObj>> findAllNew() {
        Observable.OnSubscribe<List<AnswerObj>> subscribe = new Observable.OnSubscribe<List<AnswerObj>>() {
            @Override
            public void call(Subscriber<? super List<AnswerObj>> subscriber) {
                List<AnswerObj> answerObjList = AnswerDAO.getInstance().findAllByDirty(Dirty.NEW);
                if(answerObjList == null || answerObjList.size() == 0){
                    subscriber.onError(new Throwable("Nenhuma resposta salva."));
                    return;
                }

                subscriber.onNext(answerObjList);
                subscriber.onCompleted();
            }

        };

        return Observable.create(subscribe);
    }
}
