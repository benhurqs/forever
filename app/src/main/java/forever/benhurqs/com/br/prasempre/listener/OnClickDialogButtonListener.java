package forever.benhurqs.com.br.prasempre.listener;

/**
 * Created by Benhur on 11/11/16.
 */

public interface OnClickDialogButtonListener {
    void onClickSend(String answer);
    void onClickSkip();
}
