package forever.benhurqs.com.br.prasempre.data.local;

import forever.benhurqs.com.br.prasempre.data.UserDataContract;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Benhur on 13/11/16.
 */

public class UserCache implements UserDataContract{

    private static UserCache instance;

    public static UserCache getInstance() {
        if (instance == null) {
            instance = new UserCache();
        }

        return instance;
    }
    /**
     * Save user local
     *
     * @param user
     * @return Observable
     */
    @Override
    public Observable<UserObj> save(final UserObj user) {
        Observable.OnSubscribe<UserObj> subscribe = new Observable.OnSubscribe<UserObj>() {
            @Override
            public void call(final Subscriber<? super UserObj> subscriber) {
//                UserDAO.getInstance().save(user, new OnSaveCacheListener() {
//                    @Override
//                    public void onSuccess() {
//                        subscriber.onNext(user);
//                    }
//
//                    @Override
//                    public void onError(String error) {
//                        subscriber.onError(new Throwable(error));
//                    }
//                });


            }

        };

        return Observable.create(subscribe);
    }

    /**
     * get user in db local
     *
     * @param userName
     * @return Observable
     */
    @Override
    public Observable<UserObj> login(final String userName) {
        Observable.OnSubscribe<UserObj> subscribe = new Observable.OnSubscribe<UserObj>() {
            @Override
            public void call(final Subscriber<? super UserObj> subscriber) {
//                UserObj user = UserDAO.getInstance().findByUserName(userName);
//                if(user == null){
//                    return;
//                }

//                subscriber.onNext(user);
            }

        };

        return Observable.create(subscribe);
    }
}
