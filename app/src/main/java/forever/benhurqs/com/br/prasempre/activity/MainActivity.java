package forever.benhurqs.com.br.prasempre.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import forever.benhurqs.com.br.prasempre.R;
import forever.benhurqs.com.br.prasempre.activity.Challenges.ChallengeFragment;
import forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI;
import forever.benhurqs.com.br.prasempre.utils.ActivityUtils;
import forever.benhurqs.com.br.prasempre.utils.Mocks;
import rx.Observable;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Observable<String> myObservable;
    private List<String> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addFragment();

    }

    private void saveAllChallenges(){
        ChallengesAPI.getInstance().save(Mocks.getChallenges(this));
    }


    private void addFragment(){
         ChallengeFragment challengeFragment = new ChallengeFragment();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), challengeFragment, R.id.contentFrame);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



}
