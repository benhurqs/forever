package forever.benhurqs.com.br.prasempre.cache;

import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import io.realm.RealmObject;

/**
 * Created by Benhur on 16/10/16.
 */

public class CacheObj extends RealmObject {
    public String cache;
    public @Cache.Type int type;

    public CacheObj() {
    }

    public CacheObj(String cache, int type) {
        this.cache = cache;
        this.type = type;
    }

    public <T> T convertToObject(Class<T> classFile){
        return new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC).create().fromJson(cache, classFile);
    }
}
