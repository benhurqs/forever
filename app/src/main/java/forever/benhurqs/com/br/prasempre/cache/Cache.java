package forever.benhurqs.com.br.prasempre.cache;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Benhur on 16/10/16.
 */

public class Cache {
    public static final int CHALLENGE = 0;
    public static final int CHALLENGE_LIST = 1;
    public static final int ANSWERS = 2;


    @IntDef({CHALLENGE,CHALLENGE_LIST, ANSWERS})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {}
}
