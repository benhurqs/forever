package forever.benhurqs.com.br.prasempre.obj.user;

import java.util.List;

import forever.benhurqs.com.br.prasempre.listener.OnSaveCacheListener;
import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.utils.consts.Dirty;
import io.realm.Realm;

/**
 * Created by Benhur on 20/11/16.
 */

public class AnswerDAO {

    protected Realm realm;
    private static AnswerDAO instance;

    public static AnswerDAO getInstance(){
        if(instance == null){
            instance = new AnswerDAO();
        }

        return instance;
    }

    public AnswerDAO() {
        realm = Realm.getDefaultInstance();
    }

    /**
     * Save object on DataBase
     **/
    public void save(final AnswerObj object, final OnSaveCacheListener listener) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                AnswerObj newObj = bgRealm.where(AnswerObj.class)
                        .equalTo("challengeID", object.challengeID)
                        .findFirst();

                if(newObj == null){
                    newObj = bgRealm.createObject(AnswerObj.class);
                }

                newObj.challengeID = object.challengeID;
                newObj.answer = object.answer;
                newObj.dirty = Dirty.NEW;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if (listener != null) {
                    listener.onSuccess();
                }

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if (listener != null) {
                    listener.onError(error.getMessage());
                }
            }
        });
    }


    public List<AnswerObj> findAllByDirty(@Dirty.Type int dirty){
        return realm.copyFromRealm(realm.where(AnswerObj.class)
                .equalTo("dirty", dirty)
                .findAll());
    }
}
