package forever.benhurqs.com.br.prasempre.repository;

import java.util.List;

import forever.benhurqs.com.br.prasempre.data.local.AnswerCache;
import forever.benhurqs.com.br.prasempre.data.local.ChallengeCache;
import forever.benhurqs.com.br.prasempre.data.local.UserCache;
import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import rx.Observable;

/**
 * Created by Benhur on 26/10/16.
 */

public class LocalRepository implements RepositoryDataSource {

    private static LocalRepository instance;

    public static LocalRepository getInstance(){
        if(instance == null){
            instance = new LocalRepository();
        }

        return instance;
    }

    @Override
    public Observable<List<ChallengeObj>> getAllChallenges() {
        return ChallengeCache.getInstance().findAll();
    }

    @Override
    public Observable<ChallengeObj> getChallengeByKey(final String key) {
       return ChallengeCache.getInstance().findByKey(key);
    }

    @Override
    public void saveChallenge(List<ChallengeObj> challengeObj) {
        ChallengeCache.getInstance().save(challengeObj);
    }

    @Override
    public void saveChallenge(ChallengeObj challengeObj) {
        ChallengeCache.getInstance().save(challengeObj);
    }

    @Override
    public Observable<UserObj> saveUser(UserObj user) {
        return UserCache.getInstance().save(user);

    }

    @Override
    public Observable<UserObj> loginUser(UserObj user) {
//        return UserCache.getInstance().login(user.userName);
        return null;
    }

    @Override
    public Observable<AnswerObj> saveAnswer(AnswerObj answerObj){
        return AnswerCache.getInstance().save(answerObj);
    }

    @Override
    public Observable<ChallengeObj> getChallengeById(int id) {
        return ChallengeCache.getInstance().findById(id);
    }
}
