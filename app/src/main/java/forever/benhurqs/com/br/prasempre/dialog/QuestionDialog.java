package forever.benhurqs.com.br.prasempre.dialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import forever.benhurqs.com.br.prasempre.R;
import forever.benhurqs.com.br.prasempre.listener.OnClickDialogButtonListener;

/**
 * Created by Benhur on 11/11/16.
 */

public class QuestionDialog implements View.OnClickListener {

    private Context context;
    private AlertDialog alertDialog;
    private TextView title;
    private EditText answer;
    private Button btnSend, btnSkip;
    private OnClickDialogButtonListener listener;

    public static QuestionDialog createDialog(Context context, OnClickDialogButtonListener listener){
        return new QuestionDialog(context,listener);
    }

    public QuestionDialog(Context context,OnClickDialogButtonListener listener){
        this.context = context;
        this.listener = listener;
        initDialog();
    }

    public QuestionDialog setTitle(String text){
        title.setText(text);
        return this;
    }

    private void initDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final FrameLayout frameView = new FrameLayout(context);
        builder.setView(frameView);

        alertDialog = builder.create();
        LayoutInflater inflater = alertDialog.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_question, frameView);

        title = (TextView)dialoglayout.findViewById(R.id.txt_title);
        answer = (EditText)dialoglayout.findViewById(R.id.edt_answer);

        btnSend = (Button)dialoglayout.findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);
        btnSkip = (Button)dialoglayout.findViewById(R.id.btn_skip);
        btnSkip.setOnClickListener(this);

        alertDialog.setView(dialoglayout);
    }

    public void show(){
        if(alertDialog != null) {
            alertDialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send:
                listener.onClickSend(answer.getText().toString());
                break;
            case R.id.btn_skip:
                listener.onClickSkip();
                break;
        }

        hide();
    }

    public void hide(){
        alertDialog.hide();
    }
}
