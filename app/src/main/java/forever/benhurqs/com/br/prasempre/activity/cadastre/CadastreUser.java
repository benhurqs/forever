package forever.benhurqs.com.br.prasempre.activity.cadastre;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import forever.benhurqs.com.br.prasempre.R;
import forever.benhurqs.com.br.prasempre.databinding.ActivityCadastreBinding;
import forever.benhurqs.com.br.prasempre.injection.Injection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 11/11/16.
 */

public class CadastreUser extends AppCompatActivity implements CadastreContract.View {

    private ActivityCadastreBinding binding;
    private CadastreContract.Presenter mPresenter;
    private String sex = "M";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cadastre);
        mPresenter = new CadastrePresenter(this, Injection.providerUserPrepository());
    }

    @Override
    public void hideProgress() {
        binding.contentFrame.progress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        binding.contentFrame.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public String getSex() {
        return sex;
    }

    @Override
    public String getName() {
        return binding.contentFrame.edtNome.getText().toString();
    }

    @Override
    public String getEmail() {
        return binding.contentFrame.edtEmail.getText().toString();
    }

    @Override
    public void setPresenter(@NonNull CadastreContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public Activity getContext() {
        return this;
    }

    public void onClickFemale(View view){
        sex = "M";
    }


    public void onClickMale(View view){
        sex = "F";
    }

    public void onClickSave(View view){
        mPresenter.saveUser();
    }

    @Override
    public void showError(String error) {
        Snackbar.make(binding.getRoot(), error, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }
}

