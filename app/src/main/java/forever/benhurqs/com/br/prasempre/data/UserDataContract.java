package forever.benhurqs.com.br.prasempre.data;

import forever.benhurqs.com.br.prasempre.obj.UserObj;
import rx.Observable;

/**
 * Created by Benhur on 13/11/16.
 */

public interface UserDataContract {
    Observable<UserObj> save(final UserObj user);
    Observable<UserObj> login(final String userName);
}
