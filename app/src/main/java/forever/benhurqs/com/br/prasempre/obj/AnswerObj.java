package forever.benhurqs.com.br.prasempre.obj;

import com.google.firebase.database.Exclude;

import forever.benhurqs.com.br.prasempre.utils.consts.Dirty;
import io.realm.RealmObject;

/**
 * Created by Benhur on 20/11/16.
 */

public class AnswerObj extends RealmObject{
    public String answer;
    public int challengeID;
    public String device_token;
    public String user_key;

    @Exclude
    public String challengeKey;
    @Exclude
    public @Dirty.Type int dirty;
}
