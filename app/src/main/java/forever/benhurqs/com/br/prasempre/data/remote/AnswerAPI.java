package forever.benhurqs.com.br.prasempre.data.remote;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Benhur on 20/11/16.
 */

public class AnswerAPI {

    public static final String TABLE = "Answers";
    public static final String CHALLENGE_ID_COLUMN= "challengeId";
    public static final String ANSWER_COLUMN = "answer";
    public static final String USER_ID_COLUMN = "userID";

    public static AnswerAPI instance;

    public static AnswerAPI getInstance(){
        if(instance == null){
            instance = new AnswerAPI();
        }
        return instance;
    }

    /**
     *  Save answer object in DB using Parse
     * **/
    public Observable<AnswerObj> save(final AnswerObj obj){
        Observable.OnSubscribe<AnswerObj> subscribe =  new Observable.OnSubscribe<AnswerObj>() {
            @Override
            public void call(final Subscriber<? super AnswerObj> subscriber) {
//                ParseObject answerObj = new ParseObject(TABLE);
//                answerObj.put(CHALLENGE_ID_COLUMN, obj.challengeID);
//                answerObj.put(ANSWER_COLUMN, obj.answer);
//                answerObj.put(USER_ID_COLUMN, ParseUser.getCurrentUser().getObjectId());
//
//                answerObj.saveInBackground(new SaveCallback() {
//                    @Override
//                    public void done(ParseException e) {
//                        if(e == null){
//                            subscriber.onNext(obj);
//                            subscriber.onCompleted();
//                        }else{
////                            subscriber.onError(new Throwable(e.getMessage()));
//                        }
//                    }
//                });

                obj.device_token = FirebaseInstanceId.getInstance().getToken();
                obj.user_key = FirebaseAuth.getInstance().getCurrentUser().getUid();

                FirebaseDatabase.getInstance().getReference().child(TABLE).child(obj.challengeKey).push().setValue(obj);
                FirebaseDatabase.getInstance().getReference().addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        subscriber.onNext(obj);
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Utils.dLog(databaseError.getMessage());
                        subscriber.onError(new Throwable(databaseError.getMessage()));
                    }
                });


            }
        };

        return Observable.create(subscribe);
    }


}
