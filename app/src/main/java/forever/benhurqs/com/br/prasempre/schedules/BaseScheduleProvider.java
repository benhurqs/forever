package forever.benhurqs.com.br.prasempre.schedules;

import android.support.annotation.NonNull;

import rx.Scheduler;

/**
 * Created by Benhur on 22/10/16.
 */

public interface BaseScheduleProvider {

    @NonNull
    Scheduler computation();

    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();
}
