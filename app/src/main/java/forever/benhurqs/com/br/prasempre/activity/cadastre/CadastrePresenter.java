package forever.benhurqs.com.br.prasempre.activity.cadastre;

import android.support.annotation.NonNull;

import forever.benhurqs.com.br.prasempre.repository.UserRepository;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.subscriptions.CompositeSubscription;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 13/11/16.
 */

public class CadastrePresenter implements CadastreContract.Presenter {

    private CadastreContract.View mView;
    private UserRepository repository;
    @NonNull private CompositeSubscription mSubscriptions;

    public CadastrePresenter(@NonNull CadastreContract.View mView, @NonNull UserRepository userRepository) {
        this.mView = checkNotNull(mView, "view cannot be null!");;
        this.repository = checkNotNull(userRepository, "repository cannot be null!");
        mView.setPresenter(this);
        this.mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void saveUser() {
//        if(!validateData()){
//            mView.showError("All itens is required");
//            return;
//        }
//
//        mSubscriptions.clear();
//        Subscription subscription = repository.save(mView.getContext(), getUser())
//                .subscribe(new Observer<UserObj>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.e("complete","completo");
//                        mView.hideProgress();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        mView.showError(e.getMessage());
//                    }
//
//                    @Override
//                    public void onNext(UserObj user) {
//                        mView.hideProgress();
//                        mView.getContext().finish();
//                    }
//                });
//
//        mSubscriptions.add(subscription);
    }

    private boolean validateData(){
        return !Utils.isEmpty(mView.getName()) || !Utils.isEmpty(mView.getEmail());
    }

//    private UserObj getUser(){
//        UserObj user = new UserObj();
//        user.userName = mView.getName();
//        user.gender = mView.getSex();
//        user.email = mView.getEmail();

//        return user;
//    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }
}
