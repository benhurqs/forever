package forever.benhurqs.com.br.prasempre.activity.question;

import android.animation.Animator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;

import forever.benhurqs.com.br.prasempre.R;
import forever.benhurqs.com.br.prasempre.activity.Challenges.ChallengeFragment;
import forever.benhurqs.com.br.prasempre.databinding.FragmentQuestionBinding;
import forever.benhurqs.com.br.prasempre.injection.Injection;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import io.codetail.animation.ViewAnimationUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 08/01/17.
 */

public class QuestionFragment extends Fragment implements QuestionContract.View {

    private FragmentQuestionBinding binding;
    private QuestionContract.Presenter mPresenter;
    private int px, py;

    public static QuestionFragment newInstance(int px, int py) {
        QuestionFragment fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putInt("px", px);
        args.putInt("py", py);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_question, container, false);
        mPresenter = new QuestionPresenter(this.getContext() ,this, Injection.provideParseRepository());

        if(getArguments() != null) {
            px = getArguments().getInt("px", -1);
            py = getArguments().getInt("py", -1);
        }

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        iniAnimation(px,py);
        iniButtons();

        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    private void iniButtons(){
        //        mPresenter = new QuestionPresenter(this.getContext() ,this, Injection.provideParseRepository());
        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.saveAnswer(binding.edtAnswer.getText().toString());
            }
        });

        binding.btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.skipQuestion();
            }
        });

//        binding.edtAnswer.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ( (actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN ))) {
//
//                    // do something, e.g. set your TextView here via .setText()
//                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                    return true;
//                }
//                return false;
//            }
//        });
    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void showQuestion(ChallengeObj challengeObj) {
        binding.txtTitle.setText(challengeObj.question);
    }

    @Override
    public void setPresenter(QuestionContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    private void iniAnimation(final int px,final int py){
        if(px == -1 || py == -1){
            return;
        }

        binding.rlView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);
                // get the final radius for the clipping circle
                int dx = Math.max(px, binding.rlView.getWidth() - px);
                int dy = Math.max(py, binding.rlView.getHeight() - py);
                float finalRadius = (float) Math.hypot(dx, dy);

                // Android native animator
                Animator animator =
                        ViewAnimationUtils.createCircularReveal(binding.rlView, px, py, 0, finalRadius);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.setDuration(500);
                animator.start();
            }
        });


    }

    @Override
    public void showChallenge() {
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFrame, ChallengeFragment.newInstance());
        ft.commit();
    }
}
