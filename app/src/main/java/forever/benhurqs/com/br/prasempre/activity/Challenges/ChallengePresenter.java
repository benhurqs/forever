package forever.benhurqs.com.br.prasempre.activity.Challenges;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import forever.benhurqs.com.br.prasempre.preferences.UserPreference;
import forever.benhurqs.com.br.prasempre.repository.ChallengeRepository;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 21/10/16.
 */

public class ChallengePresenter implements ChallengeContract.Presenter {
    private Context context;
    private ChallengeContract.View mView;
    private ChallengeRepository challengeRepository;
    private ChallengeObj cacheChallenge;
    @NonNull
    private CompositeSubscription mSubscriptions;

    public ChallengePresenter(Context context, @NonNull ChallengeContract.View mView, @NonNull ChallengeRepository challengeRepository) {
        this.context = context;
        this.mView = checkNotNull(mView, "tasksView cannot be null!");
        this.mView.setPresenter(this);
        this.mSubscriptions = new CompositeSubscription();
        this.challengeRepository = checkNotNull(challengeRepository);
    }

    @Override
    public void loadChallenge() {
        mView.hideFinishButton();
        updateView(challengeRepository.getNextChallenge(context));
    }


    @Override
    public void subscribe() {
        updateView(challengeRepository.getCurrentChallenge(context));
        if (Utils.needSync(UserPreference.getInstance(context).getLastView())) {
            mView.showFinishButton();
        } else {
            mView.hideFinishButton();
        }
    }

    private void updateView(Observable<ChallengeObj> observer) {
        mView.showProgress();
        mSubscriptions.clear();
        Subscription subscription = observer
                .subscribe(new Observer<ChallengeObj>() {
                    @Override
                    public void onCompleted() {
                        Log.e("complete", "completo");
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getMessage());
                    }

                    @Override
                    public void onNext(ChallengeObj challengeObj) {
                        cacheChallenge = challengeObj;
                        mView.hideProgress();
                        mView.showChallenge(challengeObj);
                        saveUser();
                    }
                });

        mSubscriptions.add(subscription);

    }

    private void saveUser(){
        mSubscriptions.clear();
        Subscription subscription = challengeRepository.saveUser(context)
                .subscribe(new Observer<UserObj>() {
                    @Override
                    public void onCompleted() {
                        Log.e("complete", "completo");
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getMessage());
                    }

                    @Override
                    public void onNext(UserObj user) {
                    }
                });

        mSubscriptions.add(subscription);
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

}
