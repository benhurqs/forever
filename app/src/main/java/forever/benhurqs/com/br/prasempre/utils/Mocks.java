package forever.benhurqs.com.br.prasempre.utils;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import forever.benhurqs.com.br.prasempre.R;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;

/**
 * Created by Benhur on 16/10/16.
 */

public class Mocks {

    public static List<ChallengeObj> getChallenges(Context context){
        ChallengeObj[] categorieObjs = convertToObject(R.raw.prasempre, ChallengeObj[].class, context);
        return Arrays.asList(categorieObjs);
    }


    public static <T> T convertToObject(int resourceId, Class<T> classFile, Context context){
        return new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC).create().fromJson(getResourceAsString(context, resourceId), classFile);
    }


    private static String getResourceAsString(Context context, int resourceId){
        try {
            Resources res = context.getResources();
            InputStream in_s = res.openRawResource(resourceId);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            return new String(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
