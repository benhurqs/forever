package forever.benhurqs.com.br.prasempre.activity.Challenges;

import android.animation.Animator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.google.firebase.analytics.FirebaseAnalytics;

import forever.benhurqs.com.br.prasempre.R;
import forever.benhurqs.com.br.prasempre.activity.question.QuestionFragment;
import forever.benhurqs.com.br.prasempre.databinding.ContentMainBinding;
import forever.benhurqs.com.br.prasempre.injection.Injection;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import io.codetail.animation.ViewAnimationUtils;

import static android.R.attr.name;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 21/10/16.
 */

public class ChallengeFragment extends Fragment implements ChallengeContract.View{

    private ContentMainBinding binding;
    private ChallengeContract.Presenter mPresenter;

    public static ChallengeFragment newInstance() {
        return new ChallengeFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.content_main, container, false);

        mPresenter = new ChallengePresenter(this.getContext() ,this, Injection.provideParseRepository());
        binding.btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mPresenter.loadChallenge();
                changeToQuestion();
            }
        });


        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
        iniAnimation();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);

    }

    @Override
    public void showChallenge(@NonNull ChallengeObj challengeObj) {
        binding.setChallenge(checkNotNull(challengeObj));
        binding.txtDescription.setVisibility(View.VISIBLE);
    }

    @Override
    public void setPresenter(@NonNull ChallengeContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);

    }

    @Override
    public void showFinishButton() {
        binding.btnFinalizar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFinishButton() {
        binding.btnFinalizar.setVisibility(View.GONE);
    }

    public void changeToQuestion(){

        // get the center for the clipping circle
        int cx = (binding.btnFinalizar.getLeft() + binding.btnFinalizar.getRight()) / 2;
        int cy = (binding.btnFinalizar.getTop() + binding.btnFinalizar.getBottom()) / 2;


        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFrame, QuestionFragment.newInstance(cx, cy));
        ft.commit();
    }

    private void iniAnimation(){
        binding.rrView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);
                // get the center for the clipping circle
                int px = (binding.rrView.getLeft() + binding.rrView.getRight()) / 2;
                int py = (binding.rrView.getTop() + binding.rrView.getBottom()) / 2;

                // get the final radius for the clipping circle
                int dx = Math.max(px, binding.rrView.getWidth() - px);
                int dy = Math.max(py, binding.rrView.getHeight() - py);
                float finalRadius = (float) Math.hypot(dx, dy);

                // Android native animator
                Animator animator =
                        ViewAnimationUtils.createCircularReveal(binding.rrView, px, py, 0, finalRadius);
                animator.setInterpolator(new AccelerateDecelerateInterpolator());
                animator.setDuration(500);
                animator.start();
            }
        });


    }
}
