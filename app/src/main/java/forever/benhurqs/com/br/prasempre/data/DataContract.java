package forever.benhurqs.com.br.prasempre.data;

import java.util.List;

import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import rx.Observable;

/**
 * Created by Benhur on 26/10/16.
 */

public interface DataContract {
    Observable<List<ChallengeObj>> findAll();
    Observable<ChallengeObj> findByKey(String key);
    Observable<ChallengeObj> findById(int id);
    void save(List<ChallengeObj> challengeObj);
    void save(ChallengeObj challengeObj);
}
