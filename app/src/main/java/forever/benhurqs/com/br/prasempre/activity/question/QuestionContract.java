package forever.benhurqs.com.br.prasempre.activity.question;

import forever.benhurqs.com.br.prasempre.base.BasePresenter;
import forever.benhurqs.com.br.prasempre.base.BaseView;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;

/**
 * Created by Benhur on 08/01/17.
 */

public class QuestionContract {

    interface View extends BaseView<QuestionContract.Presenter> {
        void hideProgress();
        void showProgress();
        void showQuestion(ChallengeObj challengeObj);
        void showChallenge();

    }

    interface Presenter extends BasePresenter {
        void loadQuestion();
        void saveAnswer(String answer);
        void skipQuestion();
    }
}
