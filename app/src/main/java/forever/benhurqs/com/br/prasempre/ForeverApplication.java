package forever.benhurqs.com.br.prasempre;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Benhur on 13/10/16.
 */

public class ForeverApplication extends Application {

    private static final String APPLICATION_ID = "AX4qdErCmLHqi85ntEJknJAvcCMGzlyMmcilSetx";
    private static final String CLIENT_KEY = "RxyoMmqVLfEwCuRSBBBMtZS4zj2AhYHgMRvJYluA";

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());


        initRealm();
        initParse();

        populateLocalStorage();
    }

    private void populateLocalStorage(){
//        CacheObj obj = CacheDAO.getInstance().findByType(Cache.CHALLENGE_LIST);
//        if(obj != null && obj.convertToObject(ChallengeObj[].class).length > 0){
//            return;
//        }

        new CompositeSubscription(ChallengesAPI.getInstance().findAll().subscribe());
    }

    private void initRealm(){
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void initParse(){
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(APPLICATION_ID)
                .clientKey(CLIENT_KEY)
                .server("https://parseapi.back4app.com").build());

        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                String deviceToken = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");
                Log.d("PARCEINSTALLATIONTOKEN", "" + deviceToken);
            }
        });
    }
}
