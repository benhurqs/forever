package forever.benhurqs.com.br.prasempre.listener;

/**
 * Created by Benhur on 16/10/16.
 */

public interface OnSaveCacheListener {
    void onSuccess();
    void onError(String error);
}
