package forever.benhurqs.com.br.prasempre.activity.Challenges;

import forever.benhurqs.com.br.prasempre.base.BasePresenter;
import forever.benhurqs.com.br.prasempre.base.BaseView;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;

/**
 * Created by Benhur on 21/10/16.
 */

public class ChallengeContract {

    interface View extends BaseView<Presenter> {

        void hideProgress();
        void showProgress();
        void showFinishButton();
        void hideFinishButton();
        void showChallenge(ChallengeObj challengeObj);

    }

    interface Presenter extends BasePresenter {

        void loadChallenge();
    }
}
