package forever.benhurqs.com.br.prasempre.repository;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.NoSuchElementException;

import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import forever.benhurqs.com.br.prasempre.preferences.UserPreference;
import forever.benhurqs.com.br.prasempre.rules.UserChallengeRules;
import rx.Observable;
import rx.functions.Func1;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 26/10/16.
 */

public class ChallengeRepository {

    private static ChallengeRepository instance;

    private RepositoryDataSource localRepository;
    private RepositoryDataSource remoteRepository;

    public static ChallengeRepository getInstance(RepositoryDataSource localRepository, RepositoryDataSource remoteRepository) {
        if (instance == null) {
            instance = new ChallengeRepository(localRepository, remoteRepository);
        }

        return instance;
    }

    public ChallengeRepository(@NonNull RepositoryDataSource localRepository, @NonNull RepositoryDataSource remoteRepository) {
        this.localRepository = checkNotNull(localRepository);
        this.remoteRepository = checkNotNull(remoteRepository);
    }

    /**
     * Get Current Challenge.
     * Looking for in local and remote repository
     *
     * @param context
     * @return Observable
     */
    public Observable<ChallengeObj> getCurrentChallenge(Context context) {
        int challengeId = UserChallengeRules.getInstance(context).getCurrentChallenge();
        return getChallengeById(context, challengeId);
    }

    /**
     * Get Next Challenge.
     * Looking for in local and remote repository
     *
     * @param context
     * @return Observable
     */
    public Observable<ChallengeObj> getNextChallenge(Context context) {
        int challengeId = UserChallengeRules.getInstance(context).getLastChallenge();
        return getChallengeById(context, challengeId);
    }

    /**
     * Get Challenge by ID
     * Looking for in local and remote repository
     *
     * @param context
     * @param challengeId
     * @return
     */
    public Observable<ChallengeObj> getChallengeById(Context context,final int challengeId) {
        Observable<ChallengeObj> localObserver = localRepository.getChallengeById(challengeId)
                .flatMap(new Func1<ChallengeObj, Observable<ChallengeObj>>() {
                    @Override
                    public Observable<ChallengeObj> call(ChallengeObj challengeObj) {
                        return Observable.just(challengeObj);
                    }
                });

        Observable<ChallengeObj> remoteObserver = remoteRepository.getChallengeById(challengeId)
                .flatMap(new Func1<ChallengeObj, Observable<ChallengeObj>>() {
                    @Override
                    public Observable<ChallengeObj> call(ChallengeObj challengeObj) {
                        return Observable.just(challengeObj);
                    }
                });
        return Observable.merge(localObserver, remoteObserver).first()
                .map(new Func1<ChallengeObj, ChallengeObj>() {
                    @Override
                    public ChallengeObj call(ChallengeObj challengeObj) {
                        if (challengeObj == null) {
                            throw new NoSuchElementException("No challenge found with Id " + challengeId);
                        }
                        return challengeObj;
                    }
                });

//        return remoteObserver.first()
//                .map(new Func1<ChallengeObj, ChallengeObj>() {
//                    @Override
//                    public ChallengeObj call(ChallengeObj challengeObj) {
//                        if (challengeObj == null) {
//                            throw new NoSuchElementException("No challenge found with Id " + challengeId);
//                        }
//                        return challengeObj;
//                    }
//                });
    }

    public Observable<AnswerObj> saveAnswer(AnswerObj obj){

        Observable<AnswerObj> localObserver = localRepository.saveAnswer(obj).flatMap(new Func1<AnswerObj, Observable<AnswerObj>>() {
            @Override
            public Observable<AnswerObj> call(AnswerObj answerObj) {
                return Observable.just(answerObj);
            }
        });

        Observable<AnswerObj> remoteObserver = remoteRepository.saveAnswer(obj).flatMap(new Func1<AnswerObj, Observable<AnswerObj>>() {
            @Override
            public Observable<AnswerObj> call(AnswerObj answerObj) {
                return Observable.just(answerObj);
            }
        });

        return Observable.concat(remoteObserver, localObserver).first()
                .map(new Func1<AnswerObj, AnswerObj>() {
                    @Override
                    public AnswerObj call(AnswerObj obj) {
                        if (obj == null) {
                            throw new NoSuchElementException("No answer save");
                        }
                        return obj;
                    }
                });
    }

    public Observable<UserObj> saveUser(Context context){
        UserObj user = new UserObj();
        user.challenge_key = UserPreference.getInstance(context).getLastChallengeKey();
        user.challenge_id = UserPreference.getInstance(context).getLastChallenge();
        user.device_token = FirebaseInstanceId.getInstance().getToken();
        user.last_view = UserPreference.getInstance(context).getLastDateView();

        Observable<UserObj> saveUserObservable = remoteRepository.saveUser(user).flatMap(new Func1<UserObj, Observable<UserObj>>() {
            @Override
            public Observable<UserObj> call(UserObj userObj) {
                return Observable.just(userObj);
            }
        });

        return saveUserObservable;
    }


}
