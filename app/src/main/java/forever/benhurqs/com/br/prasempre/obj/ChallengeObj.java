package forever.benhurqs.com.br.prasempre.obj;

import com.parse.ParseObject;

import forever.benhurqs.com.br.prasempre.utils.Utils;

import static forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI.DESC_COLUMN;
import static forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI.ID_COLUMN;
import static forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI.QUESTION_COLUMN;
import static forever.benhurqs.com.br.prasempre.data.remote.ChallengesAPI.TITLE_COLUMN;

/**
 * Created by Benhur on 16/10/16.
 */

public class ChallengeObj{
    public int id;
    public String title;
    public String description;
    public String question;
    public String key;

    public static ChallengeObj convertToObj(ParseObject parseObj){
        ChallengeObj obj = new ChallengeObj();
        obj.id = parseObj.getInt(ID_COLUMN);
        obj.description = parseObj.getString(DESC_COLUMN);
        obj.title = parseObj.getString(TITLE_COLUMN);
        obj.question = parseObj.getString(QUESTION_COLUMN);
        return  obj;
    }

    public boolean descriptionIsNull(){
        if(Utils.isEmpty(description)){
            return true;
        }

        return false;
    }

}
