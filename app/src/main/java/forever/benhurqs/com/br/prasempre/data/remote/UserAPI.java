package forever.benhurqs.com.br.prasempre.data.remote;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import forever.benhurqs.com.br.prasempre.data.UserDataContract;
import forever.benhurqs.com.br.prasempre.obj.UserObj;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by Benhur on 13/11/16.
 */

public class UserAPI implements UserDataContract {

    private static final String TABLE = "Users";
    private static final String PASSWORD = "forever";
    public static final String GENDER_COLUMN = "gender";
    private static UserAPI instance;

    public static UserAPI getInstance() {
        if (instance == null) {
            instance = new UserAPI();
        }

        return instance;
    }

    /**
     * Save user in Parse
     *
     * @param user
     * @return Observable
     */
    @Override
    public Observable<UserObj> save(final UserObj user) {
        Observable.OnSubscribe<UserObj> subscribe = new Observable.OnSubscribe<UserObj>() {
            @Override
            public void call(final Subscriber<? super UserObj> subscriber) {
                if(FirebaseAuth.getInstance().getCurrentUser() == null){
                    signInAnonymously(subscriber, user);
                }else{
                    save(subscriber, user);
                }
            }
        };

        return Observable.create(subscribe);
    }

    private void save(final Subscriber<? super UserObj> subscriber, final UserObj user){
        FirebaseDatabase.getInstance().getReference().child(TABLE).child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(user);
        FirebaseDatabase.getInstance().getReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Utils.dLog("aqui - salvei");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Utils.dLog(databaseError.getMessage());
            }
        });
    }

    /**
     * Login in Parse
     *
     * @param userName
     * @return Observable
     */
    @Override
    public Observable<UserObj> login(final String userName) {
        Observable.OnSubscribe<UserObj> subscribe = new Observable.OnSubscribe<UserObj>() {
            @Override
            public void call(final Subscriber<? super UserObj> subscriber) {
            }
        };

        return Observable.create(subscribe);
    }

    public void signInAnonymously(final Subscriber<? super UserObj> subscriber,  final UserObj user){
        FirebaseAuth.getInstance().signInAnonymously()
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            save(subscriber, user);
                        }else{
                            subscriber.onError(new Throwable("Erro ao fazer login anonimo"));
                        }

                    }
                });

    }

}
