package forever.benhurqs.com.br.prasempre.activity.question;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import forever.benhurqs.com.br.prasempre.obj.AnswerObj;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.preferences.UserPreference;
import forever.benhurqs.com.br.prasempre.repository.ChallengeRepository;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Benhur on 08/01/17.
 */

public class QuestionPresenter implements QuestionContract.Presenter {

    private Context context;
    private QuestionContract.View mView;
    private ChallengeRepository challengeRepository;
    private ChallengeObj cacheChallenge;
    @NonNull
    private CompositeSubscription mSubscriptions;

    public QuestionPresenter(Context context, @NonNull QuestionContract.View mView, @NonNull ChallengeRepository challengeRepository) {
        this.context = context;
        this.mView = checkNotNull(mView, "tasksView cannot be null!");
        this.mView.setPresenter(this);
        this.mSubscriptions = new CompositeSubscription();
        this.challengeRepository = checkNotNull(challengeRepository);
    }

    @Override
    public void loadQuestion() {
        updateView(challengeRepository.getCurrentChallenge(context));
    }

    @Override
    public void skipQuestion() {
        updateView(challengeRepository.getNextChallenge(context));
        mView.showChallenge();
    }

    @Override
    public void subscribe() {
        updateView(challengeRepository.getCurrentChallenge(context));
    }

    private void updateView(Observable<ChallengeObj> observer) {
        mView.showProgress();
        mSubscriptions.clear();
        Subscription subscription = observer
                .subscribe(new Observer<ChallengeObj>() {
                    @Override
                    public void onCompleted() {
                        Log.e("complete", "completo");
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error", e.getMessage());
                    }

                    @Override
                    public void onNext(ChallengeObj challengeObj) {
                        UserPreference.getInstance(context).saveLastChallengeKey(challengeObj.key);
                        cacheChallenge = challengeObj;
                        mView.hideProgress();
                        mView.showQuestion(challengeObj);
                    }
                });

        mSubscriptions.add(subscription);

    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }

    @Override
    public void saveAnswer(String answer) {
        if(Utils.isEmpty(answer)){
            skipQuestion();
            return;
        }

        AnswerObj answerObj = new AnswerObj();
        answerObj.answer = answer;
        answerObj.challengeID = UserPreference.getInstance(context).getLastChallenge();
        answerObj.challengeKey = UserPreference.getInstance(context).getLastChallengeKey();

        mView.showProgress();
        mSubscriptions.clear();
        Subscription subscription = challengeRepository
                .saveAnswer(answerObj)
                .subscribe(new Observer<AnswerObj>() {
                    @Override
                    public void onCompleted() {
                        Utils.dLog("Salvo com sucesso");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Utils.dLog("error -> " + e.getMessage());

                    }

                    @Override
                    public void onNext(AnswerObj answerObj) {

                    }
                });

        mSubscriptions.add(subscription);
        updateView(challengeRepository.getNextChallenge(context));
        mView.showChallenge();

    }


}
