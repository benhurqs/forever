package forever.benhurqs.com.br.prasempre.injection;

import forever.benhurqs.com.br.prasempre.repository.ChallengeRepository;
import forever.benhurqs.com.br.prasempre.repository.LocalRepository;
import forever.benhurqs.com.br.prasempre.repository.ParseRepository;
import forever.benhurqs.com.br.prasempre.repository.UserRepository;
import forever.benhurqs.com.br.prasempre.schedules.BaseScheduleProvider;
import forever.benhurqs.com.br.prasempre.schedules.SchedulerProvider;

/**
 * Created by Benhur on 21/10/16.
 */

public class Injection {

    public static ChallengeRepository provideParseRepository(){
        return ChallengeRepository.getInstance(LocalRepository.getInstance(), ParseRepository.getInstance());
    }

    public static UserRepository providerUserPrepository(){
        return UserRepository.getInstance(LocalRepository.getInstance(), ParseRepository.getInstance());
    }

    public static BaseScheduleProvider provideSchedulerProvider() {
        return SchedulerProvider.getInstance();
    }
}
