package forever.benhurqs.com.br.prasempre.rules;

import android.content.Context;
import android.util.Log;

import forever.benhurqs.com.br.prasempre.activity.MainActivity;
import forever.benhurqs.com.br.prasempre.alarm.AlarmHelper;
import forever.benhurqs.com.br.prasempre.injection.Injection;
import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;
import forever.benhurqs.com.br.prasempre.preferences.UserPreference;
import forever.benhurqs.com.br.prasempre.utils.Utils;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Benhur on 10/11/16.
 */

public class UserChallengeRules {

    private UserPreference preferences;
    private static UserChallengeRules instance;
    private Context context;

    public static UserChallengeRules getInstance(Context context){
        if(instance == null){
            instance = new UserChallengeRules(context);
        }

        return instance;
    }

    public UserChallengeRules(Context context) {
        this.context = context;
        preferences = UserPreference.getInstance(context);
    }

    /**
     * Get current challenge that should be shown
     *
     * @return challengeId
     *
     */
    public int getCurrentChallenge() {
        int nextChallenge = preferences.getLastChallenge();

        if(nextChallenge == -1 || nextChallenge > 40){
            nextChallenge = 1;
            saveChallenge(nextChallenge);
        }

        return nextChallenge;
    }



    /**
     * Get next challenge that should be shown
     * check if is time to show next challenge,
     * save this value and schedule notification
     *
     * @return challengeId
     *
     */
    public int getLastChallenge() {
        int nextChallenge = preferences.getLastChallenge();

        if(nextChallenge == -1){
            nextChallenge = 1;
            saveChallenge(nextChallenge);
            return 1;
        }

        if(Utils.needSync(preferences.getLastView())){
            nextChallenge++;
            saveChallenge(nextChallenge);
        }

        return nextChallenge;
    }

    private void saveChallenge(int nextChallenge){
        preferences.saveLastChallenge(nextChallenge);
        preferences.saveLastView();
        scheduleNotification(nextChallenge);
    }


    /**
     * Find challenge by id and
     * schedule notification to 1 day
     *
     * @param challenge
     */
    private void scheduleNotification(int challenge){
        Subscription subscription = Injection.provideParseRepository().getChallengeById(context, challenge+1)
                .subscribe(new Observer<ChallengeObj>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("error",e.getMessage());
                    }

                    @Override
                    public void onNext(ChallengeObj challengeObj) {
                        new AlarmHelper.Builder(context)
                                .setContentTitle("Desafio " + (challengeObj.id) + " liberado!" )
                                .setContentText(challengeObj.title)
//                                .setTimeInHours(20)
                                .setTimeInMinutes(1)
                                .setScreenAfterClick(MainActivity.class)
                                .build();
                    }
                });

        new CompositeSubscription(subscription);



    }
}
