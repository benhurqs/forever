package forever.benhurqs.com.br.prasempre.listener;

import java.util.List;

import forever.benhurqs.com.br.prasempre.obj.ChallengeObj;

/**
 * Created by Benhur on 16/10/16.
 */

public interface OnFindChallengeCallback {
    void onSuccess(List<ChallengeObj> challengeObjList);
    void onError();
}
